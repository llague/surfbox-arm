var WebServer = require('./modules/web_server');
var LEDPixel = require('./modules/led_pixel');
var SurfController = require('./modules/surf_controller');
var five = require('./node_modules/node-pixel/node_modules/johnny-five/lib/johnny-five');
var board = new five.Board();

board.on('ready', function () {
  console.log('board ready!');
});

// Everything needs to be instantiated before the LEDPixel!

var server = new WebServer();
server.serve();

var surfController = new SurfController({
  five: five,
  board: board,
  numSpots: 4
});

surfController.on('ready', function () {
  console.log('surf controller ready!');
});

surfController.on('spotChange', function (data) {
  console.log('Spot Change:', data);
  server.io.emit('spotChange', data);
});

surfController.on('leftButtonHold', function () {
  server.io.emit('leftButtonHold');
});

surfController.on('leftButtonDown', function () {
  server.io.emit('leftButtonDown');
});

surfController.on('leftButtonUp', function () {
  server.io.emit('leftButtonUp');
});

surfController.on('rightButtonHold', function () {
  //
  //
  server.io.emit('rightButtonHold');
});

surfController.on('rightButtonDown', function () {
  server.io.emit('rightButtonDown');
});

surfController.on('rightButtonUp', function () {
  server.io.emit('rightButtonUp');
});

var pixel = new LEDPixel({
  five: five,
  board: board
});

pixel.on('ready', function () {
  pixel.setColor('rgb(255,0,0)');
  console.log('pixel ready!');
});
