var _events = require('events');
var express = require('express');
var app = express();
var path = require('path');
var port = 4040;
var bodyParser = require('body-parser');
var request = require('request');
var NetworkChecker = require('./network_checker');
var networkCheck = new NetworkChecker();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var exec = require('child_process').exec;

var WebServer = function () {
  _events.EventEmitter.call(this);
  app.use(express.static(path.join(__dirname, '../public')));
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  app.get('/', function (req, res) {
    // Decide which page to show, depending on if we have internet access,
    // or we're in AP mode.
    networkCheck.checkNetworkStatus(function (err, status) {
      if (err) {
        console.log('network status err:', err);
      }
      console.log('network status:', status);
      if (status === 'error') {
        // no network connection, so render the network selector form
        res.sendFile(path.join(__dirname, '../public/network_connect.html'));
      } else {
        res.sendFile(path.join(__dirname, '../public/home.html'));
      }
    });
  });

  app.get('/api/wifinetworks', function (req, res) {
    networkCheck.readWifiNetworks(function (error, networks) {
      res.json(networks);
    });
  });

  app.get('/api/surfreport', function (req, res) {
    var url = 'http://friendlyweekdaycrowd.com/surfreport/ypipestojson.php?location=oceanbeach';
    request(url, function (err, response, body) {
      res.json(JSON.parse(body));
    });
  });

  app.post('/', function (req, res) {
    console.log('POST route:', req.body);
    var networkName = req.body.networkName;
    var password = req.body.networkPassword;
    // make a call to set wifi config
    networkCheck.connectToWifi(networkName, password, function (err, message) {
      if (err) {
        return res.sendStatus(500);
      }
      console.log('message:', message);
      res.sendStatus(200);
    });
  });

  io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('shutdown', function () {
      exec('shutdown -h now', function (err, stdout, stderr) { console.log('shutting down.') });
    });

    socket.on('reboot', function () {
      exec('reboot', function (error, stdout, stderr) { });
    });
  });

  this.emit('ready');
};

WebServer.prototype.serve = function () {
  http.listen(port, function () {
    console.log('Server running at http://localhost:' + port);
  });
};

WebServer.prototype.io = io;

WebServer.prototype.__proto__ = _events.EventEmitter.prototype;
module.exports = WebServer;
