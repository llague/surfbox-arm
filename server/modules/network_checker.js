/*
Here is what this script needs to do:
1.  Check if it's joined any known wifi networks. If it has, we are done. Serve up 
    the normak SurfBox site.
2.  If we have not joined any known wifi connections:
      a.  Put the device in AP mode. This will allow the user to join the SurfBox wifi 
          network on their computer. Once they do that, they will 
      b.  Navigate to a URL which will allow them to configure SurfBox to join their
          wifi network. At first, I was going to show a list of available wifi networks,
          but that would require two wifi cards, since you can't scan for networks
          when in AP mode. So now it just shows a form where you type in the wifi
          network name and password.
      c.  Once their settings are saved, SurfBox will then go into regular Wifi mode 
          and attempt to join the saved wifi network. 
      d.  Once SurfBox has joined it, they can then set their computer back to their
          home wifi network, and SurfBox will reboot and run as normal.

wlan15    unassociated  Nickname:"<WIFI@REALTEK>"
          Mode:Managed  Frequency=2.437 GHz  Access Point: Not-Associated   
          Sensitivity:0/0  
          Retry:off   RTS thr:off   Fragment thr:off
          Power Management:off
          Link Quality:0  Signal level:0  Noise level:0
          Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0
          Tx excessive retries:0  Invalid misc:0   Missed beacon:0

lo        no wireless extensions.

eth0      no wireless extensions.

lxcbr0    no wireless extensions.

*/


var sys = require('sys');
var exec = require('child_process').exec;


function NetworkChecker () { }

NetworkChecker.prototype.checkNetworkStatus = function (cb) {
  // command below only looks to see if the wifi hardware has an IP
  // /sbin/ifconfig wlan12 | grep inet | wc -l
  console.log('Checking network status...');
  exec('ping -c 1 google.com > /dev/null && echo ok || echo error', function (err, stdout, stderr) {
    cb(err, stdout.toString().trim());
  });
};

NetworkChecker.prototype.readWifiNetworks = function (cb) {
  exec('nmcli d wifi list', function (err, stdout, stderr) {
    if (err) {
      console.log('Read wifi networks err:', err);
    }
    if (stderr) {
      console.log('Read wifi networks stderr:', stderr);
    }
    var out = {
      'networks': _getNetworkNames(stdout)
    }
    cb(err, JSON.stringify(out));
  });
}

NetworkChecker.prototype.connectToWifi = function (name, password, cb) {
  var command = 'nmcli d wifi connect '+name+' password '+password+' iface wlan14';
  console.log('nmcli command:', command);
  exec(command, function (err, stdout, stderr) {
    if (err) {
      console.log('Connect to wifi error: ', err);
    }
    if (stderr) {
      console.log('Connect to wifi stderr:', stderr);
    }
    console.log('Connect to wifi stdout:', stdout);
    cb(err, stdout);
  });
}

function _getNetworkNames (str) {
  var regex = /(?!ESSID:)"[\w\s]+"/g;
  var matches = str.match(regex);
  var final = [];
  matches.forEach(function (item) {
    final.push(item.replace(/\"/g, ''));
  });
  return final;
}

module.exports = NetworkChecker;
