var _events = require('events');

var LEDPixel = function (config) {
  _events.EventEmitter.call(this);
  this.pixel = require('../node_modules/node-pixel/lib/pixel');
  this.board = config.board;
  this.strip = null;

  this.board.on('ready', function () {
    this.strip = new this.pixel.Strip({
      data: 6,
      length: 40,
      board: this.board
    });

    /**
     * Sets the pixel display color.
     * @param color {String} in 'rgb(255,0,0)'
     */
    this.setColor = function (color) {
      if (typeof color !== 'string') {
        throw new Error('Color must be in "rgb(X,X,X)" format!');
      }
      this.strip.color('rgb(0,0,0)');
      this.strip.color(color);
      this.strip.show();
    }.bind(this);

    this.demo = function () {
      setInterval(function () {
        var randR = Math.floor(Math.random() * 255);
        var randG = Math.floor(Math.random() * 255);
        var randB = Math.floor(Math.random() * 255);
        this.strip.color('rgb('+randR+','+randG+','+randB+')');
        this.strip.show();
      }.bind(this), 500);
    }.bind(this);

    this.strip.color('rgb(0,0,0)');
    this.strip.color('rgb(10,10,10)');
    this.strip.show();

    this.emit('ready');
  }.bind(this));
};

LEDPixel.prototype.__proto__ = _events.EventEmitter.prototype;

module.exports = LEDPixel;
