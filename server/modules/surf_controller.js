/*
 * SurfSpotController handles a physical connection to a potentiometer and push button
 * which are used to select surf spots and the different report views.
 * Potentiometer can range from 0-1023.
 */
var _events = require('events');

var SurfController = function (config) {
  var self = this;
  _events.EventEmitter.call(this);
  this.config = config;
  this.numSpots = config.numSpots;
  this.five = config.five;
  this.board = config.board;
  this.pot = null;

  this.board.on('ready', function () {
    this.pot = new this.five.Sensor({
      pin: 'A3',
      freq: 50
    });

    this.board.repl.inject({
      pot: this.pot
    });

    this.threshold = Math.floor(1023 / this.numSpots);
    this.spot = -1;

    this.pot.on('data', function () {
      for (var i = 0; i < self.numSpots; i++) {
        if (this.value >= (self.threshold * i) && this.value < (self.threshold * (i+1))) {
          if (self.spot !== i) {
            self.spot = i;
            self.emit('spotChange', { spot: i });
          }
          break;
        }
      }
    });

    this.leftButton = new this.five.Button(9);

    this.board.repl.inject({
      button: this.leftButton
    });

    this.leftButton.on('down', function () {
      this.emit('leftButtonDown');
    }.bind(this));

    this.leftButton.on('up', function () {
      this.emit('leftButtonUp');
    }.bind(this));

    this.leftButton.on('hold', function () {
      this.emit('leftButtonHold');
    }.bind(this));

    this.rightButton = new this.five.Button(8);

    this.board.repl.inject({
      button: this.rightButton
    });

    this.rightButton.on('down', function () {
      this.emit('rightButtonDown');
    }.bind(this));

    this.rightButton.on('up', function () {
      this.emit('rightButtonUp');
    }.bind(this));

    this.rightButton.on('hold', function () {
      this.emit('rightButtonHold');
    }.bind(this));

    this.emit('ready');
  }.bind(this));
};

SurfController.prototype.init = function () {

};

SurfController.prototype.__proto__ = _events.EventEmitter.prototype;

module.exports = SurfController;