var sys = require('sys');
var exec = require('child_process').exec;
// This command will connect to wifi: nmcli d wifi connect FriendlyWeekdayCrowd password vwM\$7qa9 iface wlan12

function WifiReader () { }

WifiReader.prototype.readNetworks = function (cb) {
  exec('iwlist wlan14 scan', function (err, stdout, stderr) {
    if (err) {
      console.log('Read wifi networks err:', err);
    }
    if (stderr) {
      console.log('Read wifi networks stderr:', stderr);
    }
    var out = {
      'networks': _getNetworkNames(stdout)
    }
    cb(err, JSON.stringify(out));
  });
}

//FriendlyWeekdayCrowd, vwM\$7qa9
WifiReader.prototype.connectToWifi = function (name, password, cb) {
  exec('nmcli d wifi connect '+name+' password '+password+' iface wlan13', function (err, stdout, stderr) {
    if (err) {
      console.log('Connect to wifi error: ', err);
    }
    if (stderr) {
      console.log('Connect to wifi stderr:', stderr);
    }
    console.log('Connect to wifi stdout:', stdout);
    cb(err, stdout);
  });
}

function _getNetworkNames (str) {
  var regex = /(?!ESSID:)"[\w\s]+"/g;
  var matches = str.match(regex);
  var final = [];
  matches.forEach(function (item) {
    final.push(item.replace(/\"/g, ''));
  });
  return final;
}

module.exports = WifiReader;
