var gulp = require('gulp');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watchify = require('watchify');
var browserify = require('browserify');
var sass = require('gulp-ruby-sass');


// add any other browserify options or transforms here
var bundler = watchify(browserify('./src/js/index.js', watchify.args));
bundler.on('update', bundle);
bundler.transform('brfs');

gulp.task('js', bundle);
gulp.task('sass', runSass);
gulp.task('default', ['js', 'sass', 'watch']);

gulp.task('watch', function () {
  gulp.watch(['./src/js/**/*.js'], ['js']);
  gulp.watch(['./src/scss/*.scss'], ['sass']);
});


function bundle() {
  return bundler.bundle()
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('bundle.js'))
    // optional, remove if you dont want sourcemaps
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
    .pipe(sourcemaps.write('./')) // writes .map file
    .pipe(gulp.dest('./public/js'));
}

function runSass () {
  gulp.src('./src/scss/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./public/css'))
}