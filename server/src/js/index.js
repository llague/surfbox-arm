var SurfReport = require('./data_SurfReport');
var GoogleMap = require('./ui_GoogleMap');
var SurfInfo = require('./ui_SurfInfo');
var ShutdownPopover = require('./ui_ShutdownPopover');

var _socket;
var _surfData = new SurfReport();
var _map = new GoogleMap();
var _info = new SurfInfo();
var _shutdownPopover = new ShutdownPopover();
var _locations = ['bolinas', 'sf', 'pacifica', 'halfmoonbay', 'santacruz'];
var _isShowingShutdownPopover = false;


function handleRightButtonHold (event) {
  if (!_isShowingShutdownPopover) {
    _shutdownPopover.show();
    _shutdownPopover.highlight(0);
    _isShowingShutdownPopover = true;
  }
}

function handleRightButtonDown (event) {
  if (_isShowingShutdownPopover) {
    _shutdownPopover.highlightNext();
  }
}

function handleRightButtonUp (event) {
  console.log('right button up');
}

function handleLeftButtonHold (event) {
  console.log('left button hold');
}

function handleLeftButtonDown (event) {
  // Execute action, if popup is showing
  if (_isShowingShutdownPopover) {
    // Select the chosen option
    var action = _shutdownPopover.getSelectedOption().option;
    switch (action) {
      case 'shutdown':
        console.log('Emitting shutdown');
        _socket.emit('shutdown');
        break;

      case 'reboot':
        console.log('Emitting reboot');
        _socket.emit('reboot');
        break;

      case 'cancel':
        console.log('Canceling popover');
        break;
    }

    // Always hide the popover no matter which action is chosen.
    _shutdownPopover.hide();
    _isShowingShutdownPopover = false;
  }
}

function handleLeftButtonUp (event) {
  console.log('left button up');
}

_map.on('GoogleMapPanDone', function () {
  console.log('pan done.');
});

_map.on('GoogleMapPanStarted', function () {
  console.log('pan started.');
});

_map.on('GoogleMapsLoaded', function () {
  _surfData.load();
});

_surfData.on('SurfDataError', function (err) {
  console.log('Surf data error:', err);
});

_surfData.on('SurfReportLoaded', function (data) {
  console.log('Surf data loaded:', data);
  _info.update(data);

  _socket = io();
  _socket.on('spotChange', function (event) {
    _map.panTo(_locations[event.spot]);
  });

  _socket.on('rightButtonHold', handleRightButtonHold);
  _socket.on('rightButtonDown', handleRightButtonDown);
  _socket.on('rightButtonUp', handleRightButtonUp);
  _socket.on('leftButtonHold', handleLeftButtonHold);
  _socket.on('leftButtonDown', handleLeftButtonDown);
  _socket.on('leftButtonUp', handleLeftButtonUp);
});

window.addEventListener('DOMContentLoaded', function () {
  setTimeout(function () {
    _map.init();
  }, 1000);

  document.body.addEventListener('keypress', function (event) {
    var key = event.keyCode || event.which;
    switch (key) {
      case 115: // S key
        handleRightButtonHold(event);
        break;

      case 107: // K key
        handleRightButtonDown(event);
        break;

      case 108: // L key
        handleLeftButtonDown(event);
        break;

    }
  });
});
