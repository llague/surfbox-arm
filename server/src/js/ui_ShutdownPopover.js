var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');


var ShutdownPopover = function () {
  this.el = document.querySelector('.js-popover-shutdown');
  this.numOptions = 3;
  this.activeIndex = 0;
};

inherits(ShutdownPopover, EventEmitter);

ShutdownPopover.prototype.forEach = function (array, callback, scope) {
  for (var i = 0; i < array.length; i++) {
    callback.call(scope, i, array[i]); // passes back stuff we need
  }
};

ShutdownPopover.prototype.show = function () {
  this.el.classList.remove('hidden');
};

ShutdownPopover.prototype.hide = function () {
  this.el.classList.add('hidden');
};

ShutdownPopover.prototype.highlight = function (num) {
  var lis = this.el.querySelectorAll('[data-index]');
  this.forEach(lis, function (index, value) {
    value.classList.remove('on');
    var intIndex = parseInt(value.dataset.index);
    if (num === intIndex) {
      value.classList.add('on');
      this.activeIndex = intIndex;
      this.activeOption = value.dataset.option;
    }
  }.bind(this), this);
};

ShutdownPopover.prototype.highlightNext = function () {
  var num = this.activeIndex + 1;
  num = num % this.numOptions;
  this.highlight(num);
};

ShutdownPopover.prototype.getSelectedOption = function () {
  return {
    option: this.activeOption,
    index: this.activeIndex
  };
};

module.exports = ShutdownPopover;