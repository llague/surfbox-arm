var EventEmitter = require('events').EventEmitter;
var request = require('browser-request');
var inherits = require('inherits');

inherits(NetworkStatus, EventEmitter);
module.exports = NetworkStatus;

var NetworkStatus = function () {

}

NetworkStatus.prototype.load = function () {
  request('/api/wifinetworks', function (err, res, body) {
    if (err) {
      return this.emit('NetworkStatusError', err);
    }
    this.emit('NetworkStatusLoaded', body);
  });
}
