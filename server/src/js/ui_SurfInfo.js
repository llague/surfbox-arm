var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');


var SurfInfo = function () {
  this.container = document.querySelector('.report');
  this.location = document.querySelector('.js-location');
  this.wave = document.querySelector('.js-wave');
  this.wind = document.querySelector('.js-wind');
  this.water = document.querySelector('.js-water');
  this.sunrise = document.querySelector('.js-sunrise');
  this.sunset = document.querySelector('.js-sunset');
};

inherits(SurfInfo, EventEmitter);

SurfInfo.prototype.update = function (data) {
  this.container.classList.remove('hidden');
  this.location.innerText = data.location;
  this.wave.innerText = data.minWaveHeight + '-' + data.maxWaveHeight + 'ft. ' + data.waveDirection;
  this.wind.innerText = data.windSpeed + ' mph ' + data.windDirection;
  this.water.innerText = data.waterTemp + ' deg';
  this.sunrise.innerText = data.sunrise;
  this.sunset.innerText = data.sunset;
};

module.exports = SurfInfo;