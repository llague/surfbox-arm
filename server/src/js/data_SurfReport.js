var EventEmitter = require('events').EventEmitter;
var request = require('browser-request');
var inherits = require('inherits');


var SurfReport = function () { };

inherits(SurfReport, EventEmitter);

SurfReport.prototype.load = function () {
  request('/api/surfreport', function (err, res, body) {
    if (err) {
      return this.emit('SurfReportError', err);
    }
    this.emit('SurfReportLoaded', JSON.parse(body));
  }.bind(this));
};

module.exports = SurfReport;
