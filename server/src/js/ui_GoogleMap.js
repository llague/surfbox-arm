var EventEmitter = require('events').EventEmitter;
var inherits = require('inherits');


var GoogleMap = function () {
  this._locations = {
    bolinas: {
      lat: 37.85385342967903,
      lng: -122.59501602172851
    },
    sf: {
      lat: 37.76278205427047,
      lng: -122.52480651855468
    },
    pacifica: {
      lat: 37.61940424245034,
      lng: -122.52068664550781
    },
    halfmoonbay: {
      lat: 37.48399099917816,
      lng: -122.49536659240722
    },
    santacruz: {
      lat: 37.10434831079968,
      lng: -122.3248210144043
    }
  };

  this._styles = [
    {
      "featureType": "administrative",
      "stylers": [
        { "visibility": "off" }
      ]
    },{
      "featureType": "poi",
      "stylers": [
        { "saturation": -100 },
        { "lightness": -30 },
        { "visibility": "off" }
      ]
    },{
      "featureType": "landscape",
      "stylers": [
        { "saturation": -100 },
        { "lightness": -41 }
      ]
    },{
      "featureType": "road",
      "stylers": [
        { "visibility": "off" }
      ]
    },{
      "featureType": "transit",
      "stylers": [
        { "visibility": "off" }
      ]
    },{
      "featureType": "water",
      "stylers": [
        { "saturation": -86 },
        { "hue": "#0099ff" },
        { "lightness": -70 }
      ]
    }
  ];
};

inherits(GoogleMap, EventEmitter);

GoogleMap.prototype.init = function () {
  // AIzaSyCiRD5p7kTA-jwDD321WsGUQvzuCZ5tPwo

  this._container = document.querySelector('.js-map');

  this._map = new google.maps.Map(this._container, {
    center: { lat: this._locations.bolinas.lat, lng: this._locations.bolinas.lng },
    zoom: 14,
    draggable: true,
    disableDefaultUI: true,
    styles: this._styles
  });
  this._map.setMapTypeId(google.maps.MapTypeId.TERRAIN);
  google.maps.event.addListenerOnce(this._map, 'idle', function (event) {
    this.emit('GoogleMapsLoaded');
  }.bind(this));
};

GoogleMap.prototype.panTo = function (location) {
  this.emit('GoogleMapPanStarted');
  var loc = this._locations[location];
  this._map.setCenter(new google.maps.LatLng(loc.lat, loc.lng));
};

module.exports = GoogleMap;
