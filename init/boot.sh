#!/bin/sh

/usr/bin/unclutter &
/usr/bin/matchbox-window-manager &
/usr/bin/chromium-browser --app http://localhost:4040 --incognito --kiosk --user-data-dir /home/odroid/chromedata
