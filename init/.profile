# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n

# run surfbox

/usr/local/bin/node /root/surfbox-arm/server/index.js &
sleep 3s
/usr/bin/xinit /root/surfbox-arm/init/boot.sh