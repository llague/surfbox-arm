#!/bin/sh

# BEFORE RUNNING THIS SCRIPT
# Make sure you've run through sudo odroid-utility.sh and done the following: 
# - expanded the filesystem
# - Set screen resolution to 1280x720
# - Disabled Xorg to login is to terminal and not desktop
# - Changed the hostname
# - Run through any updates.


# This script needs to be run as root.
if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user to run surfbox-install." 2>&1
  exit 1
fi

# Update system
echo "Updating the system"
apt-get update && apt-get upgrade
echo "Done updating the system!"

# install nodejs and dependencies 
# http://forum.odroid.com/viewtopic.php?f=52&t=3398
echo "Installing Node.js"
apt-get -y install python g++ make auto-apt checkinstall fakeroot build-essential git-core
wget http://nodejs.org/dist/v0.10.36/node-v0.10.36.tar.gz
tar xzvf node-v0.10.36.tar.gz
cd node-v0.10.36
./configure --without-snapshot
make
make install
echo "Done installing Node.js"

echo "Installing Node.js nodemon"
npm install -g nodemon

# Hide the mouse cursor
echo "Installing unclutter"
apt-get install -y unclutter

# Install matchbox window manager
echo "Installing Matchbox"
apt-get install -y matchbox

# Install VNC server. This isn’t necessary but could be helpful
apt-get install -y tightvncserver

# update for usb audio. Make sure usb audio is plugged in
# https://learn.adafruit.com/usb-audio-cards-with-a-raspberry-pi/updating-alsa-config
# edit /etc/modprobe.d/alsa-base.conf
echo "Updating audio for USB sound card playback"
mv /etc/modprobe.d/alsa-base.conf /etc/modprobe.d/alsa-base-backup.conf
cp init/alsa-base.conf /etc/modprobe.d/
cp init/asoundrc ~/.asoundrc
# create ~/.asoundrc

# create update-rc.d boot script to start server, launch browser in kiosk mode
echo "Creating SurfBox boot script"
mkdir /home/odroid/chromedata
chmod 666 /home/odroid/chromedata
cp surfbox.init /etc/init.d/surfbox
chmod a+x /etc/init.d/surfbox
update-rc.d /etc/init.d/surfbox defaults

# Make odroid user auto login from terminal.
# Make sure you have disabled Xorg so you login to terminal rather than desktop
echo "Enabling auto login for odroid user"
mv /etc/init/tty1.conf /etc/init/tty1.conf.ll_backup
cp init/tty1.conf /etc/init
chmod a+x /etc/init/tty1.conf

# Move the .profile file to ~
echo "Copying .profile to /root/.profile"
cp /root/.profile /root/.profile_backup
rm /root/.profile
cp init/.profile /root
chmod a+x /root/.profile

# boot script should detect if there is internet connectivity, and what type. 
# If it's Ethernet, it should prompt user to configure wifi. 
# Wifi configure happens via a web form and nmcli. 
echo "Done creating SurfBox. Rebooting."
reboot