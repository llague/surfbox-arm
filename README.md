**SurfBox

SurfBox is a physical device for the surf report. 
It shows surf reports for the SF Bay Area in a scrollable interface, wakes you when the surf is good, and can alert your friends when you're going out. It has tactile buttons.

This repo contains everything needed for a fresh install of SurfBox. SurfBox hardware is an [Odroid C-1](http://www.hardkernel.com/main/products/prdt_info.php?g_code=G141578608433) running Ubuntu, an Arduino UNO clone to control the hardware input, a USB audio adapter and USB powered speakers. There are also assorted potentiometers and buttons, and a power supply.

Software is Ubuntu operating system running a Node.js server and assorted Bash scripts to start services and handle system configuration. The Arduino is running Firmata, and communicates with [Johnny Five](https://github.com/rwaldron/johnny-five) which is running via the Node.js server.

SurfBox has the following features:
- Displays the surf report for several Bay Area locations via a scrollable interface
- Checks the report automatically once a day, and decides whether it should wake you up for the conditions or not. You can also configure it to alert you daily at a specified time with the report.
- On-demand surf report via a button press.
- Can send alerts to your friends to let them know you're going surfing.

The boot sequence is saved in ~/.profile on the root user of the Odroid.
